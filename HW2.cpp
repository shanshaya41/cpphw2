#include <iostream>
#include "Helpers.h"

using namespace std;

int main()
{
    int a, b;
    cout << "Enter number a: ";
    cin >> a;
    cout << "Enter number b: ";
    cin >> b;

    cout << "Square of the sum of the numbers " << a << " and " << b << " = " << SumSquare(a, b);
    return 0;
}
